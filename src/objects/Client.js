import ioClient from 'socket.io-client';

class Client {

	constructor(){
		this.io = ioClient("http://localhost:8000");
	}

	sendTest(){
	    console.log("test sent");
	    this.io.emit('test');
	}

	askNewPlayer(){
    	this.io.emit('newplayer');
	}

	sendClick(x, y){
		this.io.emit('click',{x:x, y:y});
	}

	newPlayer(){
		this.io.on('newplayer', function(data){
			this.newPlayerId = data.id;
			this.newPlayerX = data.x;
			this.newPlayerY = data.y;
		})
	}

}

export default Client;