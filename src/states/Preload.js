class Preload extends Phaser.State {
	
	preload(){
		this.load.tilemap('map', 'assets/map/example_map.json', null, Phaser.Tilemap.TILED_JSON);
	    this.load.spritesheet('tileset', 'assets/map/tilesheet.png', 32, 32);
	    this.load.image('sprite','assets/sprites/sprite.png');
	}

	create(){
		console.log('Preload');
		this.state.start('GameTitle');
	}
}

export default Preload;