class Boot extends Phaser.State {
	init(){
		this.stage.disableVisibilityChange = true;
	}

	create(){
		console.log("Boot");
		this.state.start('Preload');
	}

}

export default Boot;
